// fetch

const delay = ms => {
    return new Promise(r => setTimeout(() => r(), ms))
}
const url = 'https://jsonplaceholder.typicode.com/albums'

function fetchAlbums() {
    console.log('Fetch albums started...')
    return delay (2000)
        .then(() => fetch(url))
        .then(response => response.json())
}
fetchAlbums()
    .then(data => {
        console.log('Data:', data)
    })
    .catch(e => console.error(e))


// async await

async function fetchAsyncAlbums() {
    console.log('Fetch albums started...')
    try {
        await delay(2000)
        const response = await fetch(url)
        const data = await response.json()
        console.log('Data:', data)
    }   catch (e) {
        console.error(e)
    }
}
fetchAsyncAlbums()
