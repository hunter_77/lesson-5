console.log('Request data...')
const prom = new Promise(function(resolve, reject) {
    setTimeout(() => {
        console.log('Preparing data...')
        const data = {
            server: 'aws',
            port: 2000,
            status: 'pending'
        }
        resolve(data)
    },2000)
}, 2000)

prom.then(data => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            data.status = "success"
            resolve(data)
        }, 2000)
    }, 2000)
}).then(srcData => {
    console.log('Data received', srcData)
})